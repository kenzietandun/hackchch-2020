import Vue from "vue";
import App from "./App.vue";
import VueRouter from "vue-router";
import vuetify from "./plugins/vuetify";

Vue.config.productionTip = false;

Vue.use(VueRouter);

const routes = [
  { path: "/register", component: () => import("@/views/Register") },
  { path: "/bus", component: () => import("@/components/BusTimetable") },
  { path: "/", component: () => import("@/views/Dashboard") },
  { path: "/leaderboard", component: ()=> import("@/views/Leaderboard") }
];

const router = new VueRouter({
  routes, // short for `routes: routes`
});

new Vue({
  vuetify,
  router,
  render: (h) => h(App),
}).$mount("#app");
